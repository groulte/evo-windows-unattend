#############################
# Définition des conteneurs #
#############################

param(	
	[String]$DC1_nom_dom,
	[String]$DC1_nom_dom_NB,
	[String]$DC1_niv_foret,
	[String]$DC1_niv_dom,
	[String]$DC1_install_dns,
	[String]$DC1_deleg_dns,
	[String]$DC1_safeadminpass,
	[String]$ip_netmask,
	[String]$IPAddress
)

$date = (Get-Date).tostring("yyyy-MM-dd_HH\Hmm")

###################
# Fichiers de log #
###################


if($log.length -eq 0)
		{
		New-Item -ItemType file -Path C:\SCRIPT\Logs\ -name ($env:computername + "_" + $date + ".txt") -force
		$log = ("C:\SCRIPT\Logs\" + $env:computername + "_" + $date + ".txt")
		}


New-Item -ItemType file -Path C:\SCRIPT\Logs\ -name ($env:computername + "_" + $date + ".log") -force
$trans = ("C:\SCRIPT\Logs\" + $env:computername + "_" + $date + ".log")

Start-Transcript -path $trans


$alphanum = "^[a-zA-Z0-9]+$" # Regex qui contient les caractères alphanumériques (pour vérif. nom NetBios)
$alphanumdash = "^[a-zA-Z0-9]+([-\.][a-zA-Z0-9]+)*$" # Regex : tout ce qui commence par alphanum, autorise ensuite les -, mais pas successifs (pour vérif. nom FQDN)

$ip_netmask = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.netmask" 2>&1 | Out-String
$ip_netmask = $ip_netmask -replace "`t|`n|`r",""

$IPAddress = Test-Connection -ComputerName (hostname) -Count 1  | Select -ExpandProperty IPV4Address
$IPAddress.IPAddressToString


function toBinary ($dottedDecimal){ 
 $dottedDecimal.split(".") | %{$binary=$binary + $([convert]::toString($_,2).padleft(8,"0"))} 
 return $binary 
} 
function toDottedDecimal ($binary){ 
 do {$dottedDecimal += "." + [string]$([convert]::toInt32($binary.substring($i,8),2)); $i+=8 } while ($i -le 24) 
 return $dottedDecimal.substring(1) 
} 
 
function CidrToBin ($cidr){ 
    if($cidr -le 32){ 
        [Int[]]$array = (1..32) 
        for($i=0;$i -lt $array.length;$i++){ 
            if($array[$i] -gt $cidr){$array[$i]="0"}else{$array[$i]="1"} 
        } 
        $cidr =$array -join "" 
    } 
    return $cidr 
} 
 
# Check to see if the IP Address was entered in CIDR format 
if ($IPAddress -like "*/*") { 
    $CIDRIPAddress = $IPAddress 
    $IPAddress = $CIDRIPAddress.Split("/")[0] 
    $cidr = [convert]::ToInt32($CIDRIPAddress.Split("/")[1]) 
    if ($cidr -le 32 -and $cidr -ne 0) { 
        $ipBinary = toBinary $IPAddress 
        $smBinary = CidrToBin($cidr) 
        $ip_netmask = toDottedDecimal($smBinary) 
        } 
    else { 
        Write-Warning "Subnet Mask is invalid!" 
        Exit 
        } 
    } 
else { 
    if (!$ip_netmask) { 
        $ip_netmask = Read-Host "Netmask" 
        } 
    $ipBinary = toBinary $IPAddress 
    $smBinary = toBinary $ip_netmask 
        } 
    
 
#how many bits are the network ID 
$netBits=$smBinary.indexOf("0") 
if ($netBits -ne -1) { 
    $cidr = $netBits 
    #validate the subnet mask 
    if(($smBinary.length -ne 32) -or ($smBinary.substring($netBits).contains("1") -eq $true)) { 
        Write-Warning "Subnet Mask is invalid!" 
        Exit 
        } 
    #validate that the IP address 
    if(($ipBinary.length -ne 32) -or ($ipBinary.substring($netBits) -eq "00000000") -or ($ipBinary.substring($netBits) -eq "11111111")) { 
        Write-Warning "IP Address is invalid!" 
        Exit 
        } 
    #identify subnet boundaries 
    $networkID = toDottedDecimal $($ipBinary.substring(0,$netBits).padright(32,"0")) 
    $networkIDbinary = $ipBinary.substring(0,$netBits).padright(32,"0") 
   } 
else { 
    #identify subnet boundaries 
    $networkID = toDottedDecimal $($ipBinary) 
    } 

# On logge l'initialisation du 2ème script

Write-Output "`r`n Initialisation du 2eme script : OK `r`n" >> $log



########################
# Aide à l'utilisateur #
########################

function aide_script 
{
	Write-Output "`nUtilisation du script de création automatique d'Active Directory`n"
	Write-Output "2 arguments obligatoires : `n`n - Nom NetBios du domaine `n - Nom FQDN du domaine`n"
	Write-Output "5 arguments facultatifs : `n`n - Niveau fonctionnel forêt `n - Niveau fonctionnel domain `n - Installation DNS `n - Délégation DNS `n - Mot de passe de récupération AD`n"
	exit
}




######################################################################
#																	 #
#						TESTS SUR LES ARGUMENTS 					 #
#																	 #
######################################################################



##################################################################################################################################################################
# Si pas, ou trop, de paramètres passés, renvoie à l'aide 																										 #
# Précision : il y a 8 paramètres définis. Si on en appelle + avec le script, ils n'iront pas alimenter le compter psboundparameters.count, mais le args.count.  #
# Si on rentre dans cette boucle, on prend tous les paramètres et les arguments, et on logge (en quittant le script).											 #
##################################################################################################################################################################


if($psboundparameters.count -eq 0 -Or $args.count -ne 0) 
{
	Write-OutPut $psboundparameters `r`n $args `r`n "Aucun ou trop de paramètres passés. Fin du script" >> $log
	aide_script
}


################################
# Test des variables non vides #
################################

# On logge les tests

Write-Output "`r`n Test des variables `r`n" >> $log




# On réassigne les variables DC1_deleg_dns et DC1_install_dns en langage "système"


if($DC1_install_dns -eq "oui")
{
   $DC1_install_dns = "$true"
}

if($DC1_install_dns -eq "non")
{
   $DC1_install_dns = "$false"
}



if($DC1_deleg_dns -eq "oui")
{
   $DC1_deleg_dns = "$true"
}

if($DC1_deleg_dns -eq "non")
{
   $DC1_deleg_dns = "$false"
}


# Si nom de domaine NetBios nul, trop long ou contient autre chose que chiffres ou lettres, message d'erreur

if($DC1_nom_dom_NB.length -gt 15 -Or $DC1_nom_dom_NB.length -eq 0 -Or $DC1_nom_dom_NB -notmatch $alphanum)
{
	Write-Host "`n Le nom de domaine NetBios doit être compris entre 1 et 15 caractères. `n Caractères autorisés AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz 0123456789`n"
	Write-Output "`r`n Nom de domaine NetBios non valide `r`n" >> $log
}


# Si nom de domaine FQDN nul, trop long, commence par "-" ou contient autre chose que chiffres, lettres ou tiret, message d'erreur

if($DC1_nom_dom.length -gt 253 -Or $DC1_nom_dom.length -eq 0 -Or $DC1_nom_dom -match "-$" -Or $DC1_nom_dom -notmatch $alphanumdash)
{
	Write-Host "`n Le nom de domaine FQDN doit être compris entre 1 et 253 caractères, ne doit pas commencer par - . `n Caractères autorisés :  AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz 0123456789 -`n"
	Write-Output "`r`n Nom de domaine FQDN non valide `r`n" >> $log
}


# Si niveau fonctionnel de domaine n'est pas Win2012 ou Win2012R2, message d'erreur

if($DC1_niv_dom.length -ne 0 -And $DC1_niv_dom -ne "Win2012" -And $DC1_niv_dom -ne "Win2012R2")
{
	Write-Host "`n Le niveau fonctionnel de domaine doit être Win2012 ou Win2012R2`n"
	Write-Output "`r`n Niveau fonctionnel de domaine non valide `r`n" >> $log
}


# Si niveau fonctionnel de forêt n'est pas Win2012 ou Win2012R2, message d'erreur

if($DC1_niv_foret.length -ne 0 -And $DC1_niv_foret -ne "Win2012" -And $DC1_niv_foret -ne "Win2012R2")
{
	Write-Host "`n Le niveau fonctionnel de forêt doit être Win2012 ou Win2012R2`n"
	Write-Output "`r`n Niveau fonctionnel de forêt non valide `r`n" >> $log
}


# Si l'installation DNS n'est pas "oui" ou "non", message d'erreur

if($DC1_install_dns.length -ne 0 -And $DC1_install_dns -ne "$true" -And $DC1_install_dns -ne "$false")
{
	Write-Host "`n La valeur d'installation du DNS doit être "oui" ou "non"`n"
	Write-Output "`r`n Valeur d'installation du DNS non valide `r`n" >> $log
}


# Si la délégation DNS n'est pas "oui" ou "non", message d'erreur

if($DC1_deleg_dns.length -ne 0 -And $DC1_deleg_dns -ne "$true" -And $DC1_deleg_dns -ne "$false")
{
	Write-Host "`n La valeur de délégation du DNS doit être "oui" ou "non"`n"
	Write-Output "`r`n Valeur de délégation du DNS non valide `r`n" >> $log
}


# On logge la complétion des tests

Write-Output "`r`n OK `r`n" >> $log


#############################################################
# Assignation de valeur par défaut pour les variables vides #
#############################################################


# Si le mot de passe n'est pas personnalisé, on en met un par défaut

if($DC1_safeadminpass.length -eq 0)
{
	$DC1_safeadminpass = "P@ssw0rd44"
	Write-Output "`r`n Mot de passe de recovery : Assignation de la valeur par défaut `r`n" >> $log
}



# On convertit la chaîne de mot de passe en chaîne sécurisée obligatoire pour la commande InstallADDSForest)

# Si la délégation DNS n'est pas précisée, on la met à "non" par défaut

if($DC1_deleg_dns.length -eq 0)
{
	$DC1_deleg_dns = $false
	Write-Output "`r`n Délégation DNS : Assignation de la valeur par défaut (non) `r`n" >> $log
}


# Si l'installation DNS n'est pas précisée, on installe par défaut

if($DC1_install_dns.length -eq 0)
{
	$DC1_install_dns = $true
	Write-Output "`r`n Installation DNS : Assignation de la valeur par défaut (oui) `r`n" >> $log
}

# Si le niveau fonctionnel de forêt n'est pas précisé, on en met en Win2012 par défaut

if($DC1_niv_foret.length -eq 0)
{
	$DC1_niv_foret = "Win2012"
	Write-Output "`r`n Niveau fonctionnel de la forêt : Assignation de la valeur par défaut (Win2012)`r`n" >> $log
}

# Si le niveau fonctionnel du domaine n'est pas précisé, on en met en Win2012 par défaut

if($DC1_niv_dom.length -eq 0)
{
	$DC1_niv_dom = "Win2012"
	Write-Output "`r`n Niveau fonctionnel du domaine : Assignation de la valeur par défaut (Win2012)`r`n" >> $log
}


# On logge la complétion de l'assignation

Write-Output "`r`n OK `r`n" >> $log


####################################################################
#																   #
#					INSTALLATION ACTIVE DIRECTORY 				   #
#																   #
####################################################################


# On définit le mot de passe par défaut du compte administrateur local (obligatoire)

net user administrateur P@ssw0rd44 


# Installation des rôles prérequis

Install-WindowsFeature RSAT-ADDS
Install-WindowsFeature AD-Domain-Services -IncludeManagementTools 


$safeadminsecure = ConvertTo-SecureString -AsPlainText -String $DC1_safeadminpass -force

Install-ADDSForest `
-DatabasePath 'C:\Windows\NTDS' `
-DomainMode $DC1_niv_dom `
-DomainName $DC1_nom_dom `
-DomainNetbiosName $DC1_nom_dom_NB `
-ForestMode $DC1_niv_foret `
-Force:$true `
-CreateDnsDelegation:([System.Convert]::ToBoolean($DC1_deleg_dns)) `
-LogPath 'C:\Windows\NTDS' `
-NoRebootOnCompletion:$true `
-SysvolPath 'C:\Windows\SYSVOL' `
-InstallDns:([System.Convert]::ToBoolean($DC1_install_dns)) `
-SafeModeAdministratorPassword $safeadminsecure


Install-WindowsFeature Adcs-Cert-Authority

Add-DnsServerForwarder -IPAddress 4.2.2.2 -PassThru
New-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce\" -Name "reverse_dns_zone" -Value "$pshome\powershell.exe -Command `"Add-DnsServerPrimaryZone -NetworkID $networkID/$cidr -ReplicationScope Forest`""

Write-Output "`r`n Tout s'est bien passé apparemment, fin du script, envoi des logs par mail `r`n" >> $log

Send-MailMessage -From "deploiement@code42.fr" -To "stagiaire@code42.fr" -Attachment "$log", "$trans" -Subject "Deploiement $env:computername $date"  -Body "testouille body" -SMTPServer "f.code42.fr" -Port "25"

Stop-Transcript

Shutdown /r /t 0