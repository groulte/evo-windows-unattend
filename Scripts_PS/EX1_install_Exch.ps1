###########################
# D�finition de variables #
###########################

param(  
    [String]$EX1_orga_exch, # Nom de l'organisation Exchange
    [String]$EX1_lettre_lecteur, # Lettre du lecteur sur lequel installer la BDD
    [String]$EX1_nom_BDD # Nom de la BDD
)

$alphanum = "^[a-zA-Z0-9]+$" # Regex qui contient les caract�res alphanum�riques
$alphanumdashspace = "^[a-zA-Z0-9]+([-\s][a-zA-Z0-9]+)*$" # Regex : tout ce qui commence par alphanum, autorise ensuite les -, mais pas successifs (pour v�rif. nom FQDN)
$alpha = "[a-zA-Z]" # Regex qui contient les caract�res alphab�tiques (pour v�rif. lettre lecteur)



###################
# Fichiers de log #
###################


if($log.length -eq 0)
        {
        New-Item -ItemType file -Path C:\SCRIPT\Logs\ -name ($env:computername + "_" + $date + ".txt") -force
        $log = ("C:\SCRIPT\Logs\" + $env:computername + "_" + $date + ".txt")
        }


New-Item -ItemType file -Path C:\SCRIPT\Logs\ -name ($env:computername + "_" + $date + ".log") -force
$trans = ("C:\SCRIPT\Logs\" + $env:computername + "_" + $date + ".log")

Start-Transcript -path $trans


# On logge l'initialisation du script
Write-Output "`r`n Initialisation du script Exchange : OK `r`n" >> $log


########################
# Aide � l'utilisateur #
########################


function aide_script 
{
    Write-Output "`nUtilisation du script de cr�ation automatique d'Active Directory`n"
    Write-Output "2 arguments obligatoires : `n`n - Nom NetBios du domaine `n - Nom FQDN du domaine`n"
    Write-Output "5 arguments facultatifs : `n`n - Niveau fonctionnel for�t `n - Niveau fonctionnel domain `n - Installation DNS `n - D�l�gation DNS `n - Mot de passe de r�cup�ration AD`n"
    exit
}


######################################################################
#                                                                    #
#                       TESTS SUR LES ARGUMENTS                      #
#                                                                    #
######################################################################



##################################################################################################################################################################
# Si pas, ou trop, de param�tres pass�s, renvoie � l'aide                                                                                                        #
# Pr�cision : il y a 8 param�tres d�finis. Si on en appelle + avec le script, ils n'iront pas alimenter le compter psboundparameters.count, mais le args.count.  #
# Si on rentre dans cette boucle, on prend tous les param�tres et les arguments, et on logge (en quittant le script).                                            #
##################################################################################################################################################################


if($psboundparameters.count -eq 0 -Or $args.count -ne 0) 
{
    Write-OutPut $psboundparameters `r`n $args `r`n "Aucun ou trop de param�tres pass�s. Fin du script" >> $log
    aide_script
}


################################
# Test des variables non vides #
################################

# On logge les tests

Write-Output "`r`n Test des variables `r`n" >> $log


# Si nom d'orga. Exchange, de BDD, ou de lecteur, trop long ou contient autre chose que chiffres ou lettres, message d'erreur (et on quitte le script)


if($EX1_orga_exch.length -gt 64 -Or $EX1_orga_exch.length -eq 0 -Or $EX1_orga_exch -notmatch $alphanumdashspace)
{
    Write-Host "`n Le nom d'organisation Exchange n'est pas valide. `n Caract�res autoris�s AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz 0123456789 - *espace* `n"
    Write-Output "`r`n Nom d'organisation Exchange non valide `r`n" >> $log
}

if($EX1_nom_BDD.length -gt 32 -Or $EX1_orga_exch.length -eq 0 -Or $EX1_nom_BDD -notmatch $alphanum)
{
    Write-Host "`n Le nom de la base de donn�es n'est pas valide. `n Caract�res autoris�s AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz 0123456789 `n"
    Write-Output "`r`n Nom de base de donn�es non valide `r`n" >> $log
}

if($EX1_lettre_lecteur.length -gt 1 -Or $EX1_lettre_lecteur.length -eq 0 -Or $EX1_lettre_lecteur -notmatch $alpha)
{
    Write-Host "`n Le nom de lecteur n'est pas valide. `n Caract�res autoris�s AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz `n"
    Write-Output "`r`n Nom de lecteur non valide `r`n" >> $log
}


#############################################################
# Assignation de valeur par d�faut pour les variables vides #
#############################################################


# Si le mot de passe n'est pas personnalis�, on en met un par d�faut

if($EX1_nom_BDD.length -eq 0)
{
    $EX1_nom_BDD = "Exchange_BDD_1"
    Write-Output "`r`n Base de donn�es Exchange : Assignation de la valeur par d�faut (Exchange_BDD_1)`r`n" >> $log
}

# Si le niveau fonctionnel du domaine n'est pas pr�cis�, on en met en Win2012 par d�faut

if($EX1_lettre_lecteur.length -eq 0)
{
    $EX1_lettre_lecteur = "C"
    Write-Output "`r`n Lettre du lecteur d'installation : Assignation de la valeur par d�faut (C:\)`r`n" >> $log
}


# On logge la compl�tion de l'assignation

Write-Output "`r`n OK `r`n" >> $log


# On cr�e 2 nouvelles varibles de chemin pour la BDD et les logs, bas�s sur la lettre du lecteur assign�e au-dessus

$EX1_BDD_path = ($EX1_lettre_lecteur + ":\Exchange\" + $EX1_nom_BDD + ".edb")
$EX1_Log_path = ($EX1_lettre_lecteur + ":\Exchange\Logs")



#############
# Pr�requis #
#############


Install-WindowsFeature RSAT-ADDS-Tools


Write-Output "`r`n PrepareSchema : `r`n" >> $log
echo "PrepareSchema"
C:\Exch_2013\setup.exe /ps /IAcceptExchangeServerLicenseTerms


Write-Output "`r`n PrepareAD `r`n" >> $log
echo "PrepareAD"
C:\Exch_2013\setup.exe /PrepareAD /OrganizationName:$EX1_orga_exch /IAcceptExchangeServerLicenseTerms


Write-Output "`r`n PrepareDomain `r`n" >> $log
echo "PrepareDomain"
C:\Exch_2013\setup.exe /pd /IAcceptExchangeServerLicenseTerms


Write-Output "`r`n Installation des r�les pr�requis `r`n" >> $log
echo "On installe les r�les..."
Install-WindowsFeature AS-HTTP-Activation, Desktop-Experience, NET-Framework-45-Features, RPC-over-HTTP-proxy, RSAT-Clustering, RSAT-Clustering-CmdInterface, RSAT-Clustering-Mgmt, RSAT-Clustering-PowerShell, Web-Mgmt-Console, WAS-Process-Model, Web-Asp-Net45, Web-Basic-Auth, Web-Client-Auth, Web-Digest-Auth, Web-Dir-Browsing, Web-Dyn-Compression, Web-Http-Errors, Web-Http-Logging, Web-Http-Redirect, Web-Http-Tracing, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-Lgcy-Mgmt-Console, Web-Metabase, Web-Mgmt-Console, Web-Mgmt-Service, Web-Net-Ext45, Web-Request-Monitor, Web-Server, Web-Stat-Compression, Web-Static-Content, Web-Windows-Auth, Web-WMI, Windows-Identity-Foundation


Write-Output "`r`n Initialisation du setup UCMA `r`n" >> $log
echo "UCMA en cours"
C:\Exch_2013\UcmaRuntimeSetup.exe -q


Write-Output "`r`n Installation des Filter Packs `r`n" >> $log
echo "Filter Packs"

C:\Exch_2013\FilterPack64bit.exe /quiet
C:\Exch_2013\filterpack2010sp1-kb2460041-x64-fullfile-fr-fr.exe /quiet
C:\Exch_2013\filterpacksp2010-kb2687447-fullfile-x64-fr-fr.exe /quiet

Write-Output "`r`n Lancement du setup avec ces arguments : /m:Install /Roles:ca,mb,mt /organizationname:$EX1_orga_exch /IAcceptExchangeServerLicenseTerms /InstallWindowsComponents /DBFilePath:$EX1_BDD_path /LogFolderPath:$EX1_Log_path /MdbName:$EX1_nom_BDD `r`n" >> $log
echo "Setup Exchange"

C:\Exch_2013\setup.exe /m:Install /Roles:ca,mb,mt /organizationname:$EX1_orga_exch /IAcceptExchangeServerLicenseTerms /InstallWindowsComponents /DBFilePath:$EX1_BDD_path /LogFolderPath:$EX1_Log_path /MdbName:$EX1_nom_BDD

Write-Output "`r`n Lancement du setup CU10 `r`n" >> $log
echo "Setup CU10"

New-Item "C:\Program Files\Microsoft\Exchange Server\V15\UnifiedMessaging\grammars" -type Directory

C:\Exch_2013\CU10\setup.exe /m:upgrade /IAcceptExchangeServerLicenseTerms

pause

shutdown /r /t 0