﻿#$EX1_orga_exch = 
$EX1_orga_exch = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.EX1_orga_exch" 2>&1 | Out-String
$EX1_orga_exch = $EX1_orga_exch -replace "`t|`n|`r",""

#$EX1_orga_exch = 
$EX1_nom_BDD = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.EX1_nom_BDD" 2>&1 | Out-String
$EX1_nom_BDD = $EX1_nom_BDD -replace "`t|`n|`r",""

#$EX1_orga_exch = 
$EX1_lettre_lecteur = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.EX1_lettre_lecteur" 2>&1 | Out-String
$EX1_lettre_lecteur = $EX1_lettre_lecteur -replace "`t|`n|`r",""



# On prend la date dans une variable, au format "2015-12-15 14H49"

$date = (Get-Date).tostring("yyyy-MM-dd_HH\Hmm")

# On créé un nouveau fichier texte avec le nom de la machine et la date/heure.
# On force la création du répertoire Logs, et l'écrasement d'un fichier de même nom s'il existe (peu probable)

New-Item -ItemType file -Path C:\SCRIPT\Logs\ -name ("inst_EXCH" + $env:computername + "_" + $date + ".txt") -force


# On inscrit le nom du fichier de log dans une variable, pour que ce soit + facilement manipulable par la suite

$log = ("C:\SCRIPT\Logs\inst_EXCH" + $env:computername + "_" + $date + ".txt")


# On logge l'Initialisation du script

Write-Output " Initialisation du script de definition des variables (EX1_init_install.ps1) : OK`r`n`r`n---------------------------------`r`n Reassignation des variables :" > $log



# On réassigne les variables avec la valeur "paramètre + valeur" pour pouvoir les passer en paramètre du script.
#
# Si la variable est vide, on ne le fait pas. Et la valeur n'est donc pas passée en argument.


if($EX1_orga_exch.length -ne 0)
{
    $EX1_orga_exch = "-EX1_orga_exch $EX1_orga_exch"
}

if($EX1_nom_BDD.length -ne 0)
{
    $EX1_nom_BDD = "-EX1_nom_BDD $EX1_nom_BDD"
}

if($EX1_lettre_lecteur.length -ne 0)
{
    $EX1_lettre_lecteur = "-EX1_lettre_lecteur $EX1_lettre_lecteur"
}


# On logge la complétion de la réassignation

Write-Output "                                 OK`r`n---------------------------------`r`n" >> $log



# On logge

Write-Output " On lance le 2eme script avec les parametres suivants : `r`n $EX1_orga_exch $EX1_nom_BDD $EX1_lettre_lecteur `r`n" >> $log


# On lance le 2ème script avec ". " devant pour pouvoir conserver la valeur des variables créées dans ce script.
# En l'occurence, je veux garder la valeur de $log sans le passer en paramètre, et en évitant les erreurs potentielles liées à l'heure.
# Je pense que c'est la méthode la plus simple.

Invoke-Expression ". .\EX1_install_Exch.ps1 $EX1_orga_Exch $EX1_nom_BDD $EX1_lettre_lecteur"

# Fin du script