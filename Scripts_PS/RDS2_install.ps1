﻿#############################
# Définition des conteneurs #
#############################

#$rds_broker = "Win2012/Win2012/R2 (défaut : Win2012)"
#$rds_broker = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.RDS2_broker" 2>&1 | Out-String
#$rds_broker = $rds_broker -replace "`t|`n|`r",""

#$rds_web = "Win2012/Win2012/R2 (défaut : Win2012)"
#$rds_web = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.RDS2_web" 2>&1 | Out-String
#$rds_web = $rds_web -replace "`t|`n|`r",""

#$rds_session = "Win2012/Win2012/R2 (défaut : Win2012)"
$rds_session = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.RDS2_session" 2>&1 | Out-String
$rds_session = $rds_session -replace "`t|`n|`r",""

$alphanum = "^[a-zA-Z0-9]+$" # Regex qui contient les caractères alphanumériques (pour vérif. nom NetBios)
$alphanumdash = "^[a-zA-Z0-9]+([-.][a-zA-Z0-9]+)*$" # Regex : tout ce qui commence par alphanum, autorise ensuite les -, mais pas successifs (pour vérif. nom FQDN)


########################
# Aide à l'utilisateur #
########################

function aide_script 
{
    Write-Output "`nUtilisation du script de creation automatique de serveur RDS2`n"
    Write-Output "1 argument(s) obligatoire(s) : `n`n - Serveur Broker `n - Serveur Web Access `n - Serveur de Session `n"
    exit
}


################################
# Test des variables non vides #
################################

# On logge les tests

Write-Output "`r`n Test des variables `r`n" >> $log



## Si nom de domaine NetBios nul, trop long ou contient autre chose que chiffres ou lettres, message d'erreur
#if($rds_broker.length -eq 0 -Or $rds_broker -notmatch $alphanumdash)
#{
#    Write-Host "`n Le nom de serveur Broker doit être un nom FQDN. `n Caractères autorisés AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz 0123456789 -`n"
#    Write-Output "`r`n Nom de serveur Broker non valide `r`n" >> $log
#}

#
## Si nom de domaine FQDN nul, trop long, commence par "-" ou contient autre chose que chiffres, lettres ou tiret, message d'erreur
#
#if($rds_web.length -eq 0 -Or $rds_web -notmatch $alphanumdash)
#{
#    Write-Host "`n Le nom de serveur Web Access doit être un nom FQDN. `n Caractères autorisés :  AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz 0123456789 -`n"
#    Write-Output "`r`n Nom de serveur Web Access non valide `r`n" >> $log
#}


# Si niveau fonctionnel de domaine n'est pas Win2012 ou Win2012R2, message d'erreur

if($rds_session.length -eq 0 -Or $rds_session -notmatch $alphanumdash)
{
    Write-Host "`n Le nom de serveur de Session doit être un nom FQDN. `n Caractères autorisés :  AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz 0123456789 -`n"
    Write-Output "`r`n Nom de serveur de Session non valide `r`n" >> $log
}


Write-Host "Serveur RDS2 : $rds_broker $rds_web $rds_session"

####################
# Installation RDS #
####################


# On logge le début de l'installation

Write-Output "`r`n Installation RDS `r`n" >> $log

Start-Transcript -path $log -append

Import-Module RemoteDesktop
New-RDSessionDeployment -SessionHost $rds_session
##-ConnectionBroker $rds_broker -WebAccessServer $rds_web

pause