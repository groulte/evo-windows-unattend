#############################
# Définition des conteneurs #
#############################

param(
	[String]$DC1_nom_dom,
	[String]$username
)

$DC1_nom_dom = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_nom_dom" 2>&1 | Out-String
$DC1_nom_dom = $DC1_nom_dom -replace "`t|`n|`r",""

$password = "P@ssw0rd44" | ConvertTo-SecureString -asPlainText -Force

$username = ($DC1_nom_dom + "\administrateur")

$credential = New-Object System.Management.Automation.PSCredential($username,$password)

echo "domaine : $DC1_nom_dom $username "

pause

Add-Computer -DomainName $DC1_nom_dom -Credential $credential

New-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce\" -Name "read_role" -Value "$pshome\powershell.exe -file C:\SCRIPTS\Scripts_PS\read_role.ps1" -Force
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "AutoAdminLogon" -Value "1" -Force
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "DefaultDomainName" -Value ".\" -Force
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "DefaultUserName" -Value "admin" -Force
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "DefaultPassword" -Value "P@ssw0rd44" -Force

shutdown /r /t 0

exit