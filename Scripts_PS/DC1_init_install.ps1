﻿#$DC1_niv_dom = "Win2012/Win2012/R2 (défaut : Win2012)"
$DC1_niv_dom = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_niv_dom" 2>&1 | Out-String
$DC1_niv_dom = $DC1_niv_dom -replace "`t|`n|`r",""

#$DC1_nom_dom = "domaine.test"
$DC1_nom_dom = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_nom_dom" 2>&1 | Out-String
$DC1_nom_dom = $DC1_nom_dom -replace "`t|`n|`r",""

#$DC1_nom_dom_NB = "DOMAINETEST"
$DC1_nom_dom_NB = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_nom_dom_NB" 2>&1 | Out-String
$DC1_nom_dom_NB = $DC1_nom_dom_NB -replace "`t|`n|`r",""

#$DC1_niv_foret = "Win2012/Win2012/R2 (défaut : Win2012)"
$DC1_niv_foret = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_niv_foret" 2>&1 | Out-String
$DC1_niv_foret = $DC1_niv_foret -replace "`t|`n|`r",""

#$DC1_safeadminpass = "Mot de passe de recovery AD"
$DC1_safeadminpass = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_safeadminpass" 2>&1 | Out-String
$DC1_safeadminpass = $DC1_safeadminpass -replace "`t|`n|`r",""

#$DC1_install_dns = "oui/non (défaut : oui)"
$DC1_install_dns = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_install_dns" 2>&1 | Out-String
$DC1_install_dns = $DC1_install_dns -replace "`t|`n|`r",""

#$DC1_deleg_dns = "oui/non (défaut : non)"
$DC1_deleg_dns = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_deleg_dns" 2>&1 | Out-String
$DC1_deleg_dns = $DC1_deleg_dns -replace "`t|`n|`r",""


# On prend la date dans une variable, au format "2015-12-15 14H49"

$date = (Get-Date).tostring("yyyy-MM-dd_HH\Hmm")

# On créé un nouveau fichier texte avec le nom de la machine et la date/heure.
# On force la création du répertoire Logs, et l'écrasement d'un fichier de même nom s'il existe (peu probable)

New-Item -ItemType file -Path C:\SCRIPT\Logs\ -name ($env:computername + "_" + $date + ".txt") -force


# On inscrit le nom du fichier de log dans une variable, pour que ce soit + facilement manipulable par la suite

$log = ("C:\SCRIPT\Logs\" + $env:computername + "_" + $date + ".txt")


# On logge l'Initialisation du script

Write-Output " Initialisation du script de definition des variables (DC1_init_install.ps1) : OK`r`n`r`n---------------------------------`r`n Reassignation des variables :" > $log




# On réassigne les variables avec la valeur "paramètre + valeur" pour pouvoir les passer en paramètre du script.
#
# Si la variable est vide, on ne le fait pas. Et la valeur n'est donc pas passée en argument.


if($DC1_install_dns.length -ne 0)
{
    $DC1_install_dns = "-DC1_install_dns $DC1_install_dns"
}

if($DC1_deleg_dns.length -ne 0)
{
    $DC1_deleg_dns = "-DC1_deleg_dns $DC1_deleg_dns"
}

if($DC1_safeadminpass.length -ne 0)
{
    $DC1_safeadminpass = "-DC1_safeadminpass $DC1_safeadminpass"
}
if($DC1_niv_foret.length -ne 0)
{
    $DC1_niv_foret = "-DC1_niv_foret $DC1_niv_foret"
}

if($DC1_niv_dom.length -ne 0)
{
    $DC1_niv_dom = "-DC1_niv_dom $DC1_niv_dom"
}

if($DC1_nom_dom.length -ne 0)
{
    $DC1_nom_dom = "-DC1_nom_dom $DC1_nom_dom"
}

if($DC1_nom_dom_NB.length -ne 0)
{
    $DC1_nom_dom_NB = "-DC1_nom_dom_NB $DC1_nom_dom_NB"
}


# On logge la complétion de la réassignation

Write-Output "                                 OK`r`n---------------------------------`r`n" >> $log



# On logge

Write-Output " On lance le 2eme script avec les parametres suivants : `r`n $DC1_install_dns $DC1_deleg_dns $DC1_safeadminpass $DC1_niv_foret $DC1_niv_dom $DC1_nom_dom $DC1_nom_dom_NB `r`n" >> $log


# On lance le 2ème script avec ". " devant pour pouvoir conserver la valeur des variables créées dans ce script.
# En l'occurence, je veux garder la valeur de $log sans le passer en paramètre, et en évitant les erreurs potentielles liées à l'heure.
# Je pense que c'est la méthode la plus simple.

Invoke-Expression ". C:\SCRIPTS\Scripts_PS\DC1_install_ad.ps1 $DC1_install_dns $DC1_deleg_dns $DC1_safeadminpass $DC1_niv_foret $DC1_niv_dom $DC1_nom_dom $DC1_nom_dom_NB"

# Fin du script