#$install_AD = "oui / non"
$role = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.role" 2>&1 | Out-String
$role = $role -replace "`t|`n|`r",""

#$DC1_nom_dom = "domaine.test"
$DC1_nom_dom = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.DC1_nom_dom" 2>&1 | Out-String
$DC1_nom_dom = $DC1_nom_dom -replace "`t|`n|`r",""

$domaine_actuel = (Get-WmiObject Win32_ComputerSystem).Domain

New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "AutoAdminLogon" -Value "0" -Force
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "DefaultPassword" -Value "0" -Force

If ($role -match "DC1") 

    {
    powershell -file C:\SCRIPTS\Scripts_PS\DC1_init_install.ps1
    }

ElseIf ($domaine_actuel -ne $DC1_nom_dom)

    {
    powershell -file C:\SCRIPTS\Scripts_PS\CLIENT_JOIN.ps1
    }

ElseIf ($role -match "EX1") 

    {
    powershell -file C:\SCRIPTS\Scripts_PS\EX1_init_install.ps1
    }

ElseIf ($role -match "RDS1") 

    {
    powershell -file C:\SCRIPTS\Scripts_PS\RDS1_install.ps1
    }

ElseIf ($role -match "RDS2") 

    {
    powershell -file C:\SCRIPTS\Scripts_PS\RDS2_install.ps1
    }
Else
    {
    stop-process -id $PID
    }