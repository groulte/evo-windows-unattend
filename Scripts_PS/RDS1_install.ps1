﻿#############################
# Définition des conteneurs #
#############################

#$rds_server = "Win2012/Win2012/R2 (défaut : Win2012)"
$rds_server = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.RDS1_server" 2>&1 | Out-String
$rds_server = $rds_server -replace "`t|`n|`r",""

$alphanum = "^[a-zA-Z0-9]+$" # Regex qui contient les caractères alphanumériques (pour vérif. nom NetBios)
$alphanumdash = "^[a-zA-Z0-9]+([-.][a-zA-Z0-9]+)*$" # Regex : tout ce qui commence par alphanum, autorise ensuite les -, mais pas successifs (pour vérif. nom FQDN)


########################
# Aide à l'utilisateur #
########################

function aide_script 
{
    Write-Output "`nUtilisation du script de creation automatique de serveur RDS1`n"
    Write-Output "1 argument(s) obligatoire(s) : `n`n - Serveur RDS"
    exit
}


################################
# Test des variables non vides #
################################

# On logge les tests

Write-Output "`r`n Test des variables `r`n" >> $log



# Si nom de domaine NetBios nul, trop long ou contient autre chose que chiffres ou lettres, message d'erreur

if($rds_server.length -eq 0 -Or $rds_server -notmatch $alphanumdash)
{
    Write-Host "`n Le nom de serveur RDS doit être un nom FQDN. `n Caractères autorisés AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz 0123456789 -`n"
    Write-Output "`r`n Nom de serveur RDS non valide `r`n" >> $log
}

Write-Host "Serveur RDS1: $rds_server"

####################
# Installation RDS #
####################


# On logge le début de l'installation

Write-Output "`r`n Installation RDS `r`n" >> $log

Start-Transcript -path $log -append

Import-Module RemoteDesktop
New-RDSessionDeployment -ConnectionBroker $rds_broker -WebAccessServer $rds_broker -SessionHost $rds_broker

pause

